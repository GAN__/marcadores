# Marcadores

Chequear marcadores de Fifefox. Encuentra duplicados y enlaces que no funcionan.
Desarrollado con [Lazarus](https://www.lazarus-ide.org/) y [FreePascal](http://www.freepascal.org/) en GNU/Linux.

Para compilar se necesita Lazarus 1.8.4 o superior, FreePascal 3.0 o superior y el paquete [Eye-Candy Controls](https://wiki.freepascal.org/Eye-Candy_Controls) disponible desde el gestor de paquetes en linea (OPM) del IDE Lazarus.

Licencia Copyleft de mi parte. Ver licencias de Lazarus, FreePascal y Eye-Candy Controls.

Los íconos son de https://icons8.com/

