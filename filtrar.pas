unit filtrar;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, globales,
  Buttons;

type

  { TFFiltrar }

  TFFiltrar = class(TForm)
    BAplicarFiltro: TBitBtn;
    BCancelar: TBitBtn;
    BCerrar: TBitBtn;
    Label1: TLabel;
    Memo1: TMemo;
    procedure BAplicarFiltroClick(Sender: TObject);
    procedure BCerrarClick(Sender: TObject);
  private

  public

  end;

var
  FFiltrar: TFFiltrar;

implementation

{$R *.lfm}

{ TFFiltrar }

procedure TFFiltrar.BAplicarFiltroClick(Sender: TObject);
var
  sl:TStringList;
  i,j:Integer;
begin
  sl:=TStringList.Create;
  sl.Text:=Memo1.Text;
  if sl.Count<1 then
  begin
    ShowMessage('No se ha definido ningún filtro.');
    sl.Free;
    Exit;
  end;
  for i:=Low(aReg) to High(aReg) do
  begin
    for j:=0 to sl.Count-1 do
    begin
      if Pos(sl[j],aReg[i].URL)<>0 then
      begin
        aReg[i].chequear:=False;
        aReg[i].borrar:=False;
        aReg[i].Eliminar:=False;
      end;
    end;
  end;
  sl.Free;
end;

procedure TFFiltrar.BCerrarClick(Sender: TObject);
begin
  Close;
end;

end.

