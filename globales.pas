unit globales;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TReg=record
    indice:Integer; // de ArchOrig
    chequear:Boolean;
    URL:String;
    Redirect:Integer;
    statuscode:Integer;
    borrar:Boolean; // orientativo para stringgrid    o sobra y quitarlo?
    Eliminar:Boolean;
  end;

  { TMedirTiempo }

  TMedirTiempo=class(TObject)
    private
      FInicio:TTime;
      FFinaliza:TTime;
      FTranscurrido:TTime;
    public
      constructor Create (Owner:TComponent);
      function Iniciar:TTime;
      function Finalizar:TTime;
      function Transcurrido:TTime;
  end;

  TBuscar=(Duplicados,Errores,Error0,Error400,Error500,Redirect,OK200,Todos);

  function CantidadEnlacesAChueqear:Integer;

var
  aReg:array of TReg;
  QueBuscar:TBuscar;
  Archivo:String='';
  Chequeo:Boolean=False;
  SeBorroAlgo:Boolean=False;
  nErrores:Integer;
  nDuplicados:Integer;

implementation

{ TMedirTiempo }

constructor TMedirTiempo.Create(Owner: TComponent);
begin
  FInicio:=StrToTime('0');
  FFinaliza:=StrToTime('0');
  FTranscurrido:=StrToTime('0');
end;

function TMedirTiempo.Iniciar: TTime;
begin
  FInicio:=Now;
end;

function TMedirTiempo.Finalizar: TTime;
begin
  FFinaliza:=Now;
end;

function TMedirTiempo.Transcurrido: TTime;
begin
  FTranscurrido:=FFinaliza-FInicio;
  Result:=FTranscurrido;
end;

function CantidadEnlacesAChueqear:Integer;
var
  i,cant:Integer;
begin
  cant:=0;
  for i:=Low(aReg) to High(aReg) do
    if aReg[i].chequear then Inc(cant);
  Result:=cant;
end;

end.

