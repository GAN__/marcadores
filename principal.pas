unit principal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons, fphttpclient, globales, seleccionar,
  StdCtrls, ComCtrls, Menus, ECProgressBar, ECScheme, ECSlider, openssl, filtrar;

type

  { TForm1 }

  TForm1 = class(TForm)
    BCerrar: TBitBtn;
    BChequearURLs: TBitBtn;
    BFiltros: TBitBtn;
    BGuardarLog: TBitBtn;
    BVerResultados: TBitBtn;
    BGuardar: TBitBtn;
    BGuardarComo: TBitBtn;
    BBuscarDuplicados: TBitBtn;
    BSelArchivo: TBitBtn;
    ECProgressBar1: TECProgressBar;
    mnAcercaDe: TMenuItem;
    mnCreditos: TMenuItem;
    mnInfo: TMenuItem;
    pbTotal: TECProgressBar;
    ECSlider1: TECSlider;
    lArchivoOriginal: TLabel;
    lCantURL: TLabel;
    lWeb: TLabel;
    MainMenu1: TMainMenu;
    mnArchivoNuevo: TMenuItem;
    mnArchivo: TMenuItem;
    mnAbrirArchivo: TMenuItem;
    mnGuardar: TMenuItem;
    mnGuardarComo: TMenuItem;
    mnLogs: TMenuItem;
    mnVerArchivoOriginal: TMenuItem;
    mnArchivoDepurado: TMenuItem;
    mnVerArrayAreg: TMenuItem;
    mmChequeando: TMemo;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    procedure BBuscarDuplicadosClick(Sender: TObject);
    procedure BCerrarClick(Sender: TObject);
    procedure BChequearURLsClick(Sender: TObject);
    procedure BFiltrosClick(Sender: TObject);
    procedure BGuardarClick(Sender: TObject);
    procedure BGuardarComoClick(Sender: TObject);
    procedure BGuardarLogClick(Sender: TObject);
    procedure BVerResultadosClick(Sender: TObject);
    procedure BSelArchivoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure mnAcercaDeClick(Sender: TObject);
    procedure mnArchivoDepuradoClick(Sender: TObject);
    procedure mnArchivoNuevoClick(Sender: TObject);
    procedure mnCreditosClick(Sender: TObject);
    procedure mnVerArchivoOriginalClick(Sender: TObject);
    procedure mnVerArrayAregClick(Sender: TObject);
  private
    HayRedirect:Boolean;
    RedirectNro:Integer;
    CantEnlaces:Integer;
    procedure DoOnDataReceived(Sender: TObject; const AContentLength, ACurrentPos : Int64);
    procedure ShowReridect(Sender: TObject; const sURL:String; var redirectURL:String);
    function ChequearEnlace(const enlace:String; var sc:Integer):Boolean;
    function ExtraerURL(const url:String):String;
    procedure Eliminar;
    function CargarHTMLOriginal:Boolean;
    function DepurarHTMLOriginal:Boolean;
    procedure DesHabilitoBotones;
  public
    procedure VerMemo(var sl:TStringList);
  end;

var
  Form1: TForm1;
  ArchOrig, ArchTemp, ArchNuevo:TStringList;    //ArchTemp sirve para algo? Solo para el log.

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  ArchOrig:=TStringList.Create;
  ArchTemp:=TStringList.Create;
  ArchNuevo:=TStringList.Create;
  HayRedirect:=False;
  DesHabilitoBotones;
  lArchivoOriginal.Caption:='';
  lCantURL.Caption:='';
  lWeb.Caption:='';
end;

procedure TForm1.mnArchivoDepuradoClick(Sender: TObject);
begin
 VerMemo(ArchTemp);
end;

procedure TForm1.mnArchivoNuevoClick(Sender: TObject);
begin
  VerMemo(ArchNuevo);
end;

procedure TForm1.mnVerArchivoOriginalClick(Sender: TObject);
begin
  VerMemo(ArchOrig);
end;

procedure TForm1.mnVerArrayAregClick(Sender: TObject);
var
  F:TForm;
  memo:TMemo;
  i:Integer;
begin
  F:=TForm.Create(nil);
  F.Height:=640;
  F.Width:=800;
  F.Position:=poMainFormCenter;
  memo:=TMemo.Create(F);
  memo.Parent:=F;
  memo.Align:=alClient;
  memo.ScrollBars:=ssAutoBoth;
  for i:=Low(aReg) to High(aReg) do
    memo.Lines.Add(IntToStr(aReg[i].indice)+' - '+aReg[i].URL+'CHEQUEAR: '+BoolToStr(aReg[i].chequear,'Chequear','No chequear')+' - '
    +IntToStr(aReg[i].statuscode)+' - '+IntToStr(aReg[i].Redirect)+' - '+BoolToStr(aReg[i].borrar,'Borrar','No borrar'));
  F.ShowModal;
  FreeAndNil(F);
end;

procedure TForm1.BSelArchivoClick(Sender: TObject);
begin
  if OpenDialog1.Execute then Archivo:=OpenDialog1.FileName;
  if OpenDialog1.FileName='' then Exit;
  if not(CargarHTMLOriginal) then
  begin
    ShowMessage('No se pudo cargar el archivo o el mismo está vacio.');
    Exit;
  end;
  if not(DepurarHTMLOriginal) then
  begin
    ShowMessage('No se encontraron enlaces, posiblemente no sea el archivo importado de FireFox.');
    Archivo:='';
    ArchOrig.Clear;
    SetLength(aReg,0);
    Exit;
  end;
  BBuscarDuplicados.Enabled:=True;
  BChequearURLs.Enabled:=True;
  BBuscarDuplicados.Enabled:=True;
  BVerResultados.Enabled:=False;
  BGuardar.Enabled:=False;
  BGuardarComo.Enabled:=False;
  mnGuardar.Enabled:=False;
  mnGuardarComo.Enabled:=False;
  BGuardarLog.Enabled:=True;
end;

function TForm1.CargarHTMLOriginal: Boolean;
begin
  Result:=True;
  ArchOrig.Clear;
  ArchOrig.LoadFromFile(Archivo);
  if ArchOrig.Count<1 then
  begin
    Result:=False;
    Exit;
  end;
  SetLength(aReg,0);
  lArchivoOriginal.Caption:=Archivo+' : líneas: '+IntToStr(ArchOrig.Count);
  SetLength(aReg,ArchOrig.Count);
end;

function TForm1.DepurarHTMLOriginal: Boolean;
var
  i, desde, cantURL:Integer;
  FoundDL:Boolean=False;
  temp:TStringList;
begin
  Result:=True;
  cantURL:=0;
  for i:=0 to ArchOrig.Count-1 do
    begin
      aReg[i].indice:=i;
      aReg[i].Eliminar:=False;
      aReg[i].chequear:=(LeftStr(TrimLeft(ArchOrig[i]),17)='<DT><A HREF="http');  //OK!!!!!!!!!
      if aReg[i].chequear then
      begin
        aReg[i].URL:=ExtraerURL(ArchOrig[i]);
        mmChequeando.Lines.Add(aReg[i].URL);
        Inc(cantURL);
      end;
    end;
  if cantURL<1 then
  begin
    Result:=False;
    Exit;
  end;
  i:=0;
  repeat  //averiguo la línea del primer link
    if LeftStr(ArchOrig[i],4)='<DL>' then FoundDL:=True else Inc(i);
  until FoundDL;
  temp:=TStringList.Create;
  desde:=i+1;
  for i:=desde to ArchOrig.Count-1 do //copio todo desde el primer <DL> encontrado hasta el final.
    temp.Add(Trim(ArchOrig[i]));
  ArchTemp.Clear;
  for i:=0 to temp.Count-1 do
    if LeftStr(temp[i],17)='<DT><A HREF="http' then ArchTemp.Add(temp[i]);
  CantEnlaces:=ArchTemp.Count;
  lCantURL.Caption:='Enlaces a chequear: '+IntToStr(ArchTemp.Count);
  temp.Free;
end;

procedure TForm1.VerMemo(var sl: TStringList);
var
  F:TForm;
  mm:TMemo;
begin
  F:=TForm.Create(nil);
  F.Height:=540;
  F.Width:=600;
  F.Position:=poMainFormCenter;
  mm:=TMemo.Create(nil);
  mm.Text:=sl.Text;
  mm.Visible:=True;
  mm.Parent:=F;
  mm.Align:=alClient;
  mm.ScrollBars:=ssAutoBoth;
  F.ShowModal;
  FreeAndNil(mm);
  FreeAndNil(F);
end;

procedure TForm1.BVerResultadosClick(Sender: TObject);
begin
  SeBorroAlgo:=False;
  QueBuscar:=Todos;
  FSeleccionar:=TFSeleccionar.Create(nil);
  FSeleccionar.ShowModal;
  FreeAndNil(FSeleccionar);
  if SeBorroAlgo then
    begin
      BChequearURLs.Enabled:=False;
      BBuscarDuplicados.Enabled:=False;
      BVerResultados.Enabled:=False;
      BGuardar.Enabled:=True;
      BGuardarComo.Enabled:=True;
      mnGuardar.Enabled:=True;
      mnGuardarComo.Enabled:=True;
      Eliminar;
    end;
end;

procedure TForm1.DoOnDataReceived(Sender: TObject; const AContentLength, ACurrentPos: Int64);
begin
  if AContentLength > 0 then
  begin
    ECProgressBar1.Position:=Round(100*ACurrentPos/AContentLength);
  end;
  Application.ProcessMessages;
end;

procedure TForm1.ShowReridect(Sender: TObject; const sURL: String; var redirectURL: String);
begin
  HayRedirect:=True;
  RedirectNro:=(Sender as TFPHTTPClient).ResponseStatusCode;
  mmChequeando.Lines.Add('Redirección detectada: '+IntToStr(RedirectNro));
  mmChequeando.Lines.Add(sURL+' ----> '+redirectURL);
end;

function TForm1.ExtraerURL(const url: String): String;
{<DT><A HREF="https://search.google.com/search-console/links?resource_id=https://lazarus-freepascal.blogspot.com/&
hl=es&utm_source=wmx&utm_medium=report-banner&utm_content=links" ADD_DATE="1537667447" LAST_MODIFIED="1537667447"}
var
  desde,hasta:Integer;
  aux:String;
begin
  desde:=Pos('"',url);
  aux:=Copy(url,desde+1,Length(url)); // aux = https://search.google.co....
  hasta:=Pos('"',aux)-1;
  Result:=Copy(aux,1,hasta);
end;

procedure TForm1.Eliminar;
var
  i:Integer;
begin
  for i:=Low(aReg) to High(aReg) do
    begin
      if not(aReg[i].Eliminar) then
      begin
        ArchNuevo.Add(ArchOrig[i]);
      end;
    end;
end;

procedure TForm1.BCerrarClick(Sender: TObject);
begin
  Close;
end;

procedure TForm1.BBuscarDuplicadosClick(Sender: TObject);
begin
  SeBorroAlgo:=False;
  QueBuscar:=Duplicados;
  FSeleccionar:=TFSeleccionar.Create(nil);
  FSeleccionar.ShowModal;
  FreeAndNil(FSeleccionar);
  if SeBorroAlgo then
    begin
      BChequearURLs.Enabled:=False;
      BBuscarDuplicados.Enabled:=False;
      BVerResultados.Enabled:=False;
      BGuardar.Enabled:=True;
      BGuardarComo.Enabled:=True;
      mnGuardar.Enabled:=True;
      mnGuardarComo.Enabled:=True;
      Eliminar;
    end;
end;

procedure TForm1.BChequearURLsClick(Sender: TObject);
var
  i, status:integer;
  Tiempo:TMedirTiempo;
  pbUnidad:Double;
begin
  Tiempo:=TMedirTiempo.Create(nil);
  Tiempo.Iniciar;
  nErrores:=0;
  ECProgressBar1.Caption:='Recibiendo datos.';
  pbTotal.Position:=0;
  pbTotal.Max:=100;
  pbUnidad:=100/CantEnlaces;
  status:=-1;
  mmChequeando.Clear;
  for i:=low(aReg) to High(aReg) do
  begin
    if aReg[i].chequear then
    begin
      if ((i>0) and ((i mod 100) = 0)) then
        ShowMessage('Pausa automáticas cada 100 enlaces.');
      if ChequearEnlace(aReg[i].URL,status) then
        begin
          aReg[i].borrar:=False;
        end
      else
      begin
        aReg[i].borrar:=True;
        Inc(nErrores);
      end;
      aReg[i].statuscode:=status;
      if HayRedirect then aReg[i].Redirect:=RedirectNro;
      pbTotal.Position:=pbTotal.Position+pbUnidad;
      Application.ProcessMessages;
    end;
    HayRedirect:=False;
  end;
  Tiempo.Finalizar;
  Chequeo:=True;
  ECProgressBar1.Position:=0;
  ECProgressBar1.Caption:='Finalizado.'+TimeToStr(Tiempo.Transcurrido);
  if nErrores=0 then ShowMessage('No se encontraron errores. No obstante puede buscar si hay marcadores duplicados.');
  BVerResultados.Enabled:=True;
end;

procedure TForm1.BFiltrosClick(Sender: TObject);
begin
  FFiltrar:=TFFiltrar.Create(nil);
  FFiltrar.ShowModal;
  FreeAndNil(FFiltrar);
  CantEnlaces:=CantidadEnlacesAChueqear;
  lCantURL.Caption:='Enlaces a chequear: '+IntToStr(CantEnlaces);
end;

procedure TForm1.BGuardarClick(Sender: TObject);
begin
  ArchNuevo.SaveToFile(Archivo);
end;

procedure TForm1.BGuardarComoClick(Sender: TObject);
begin
  if SaveDialog1.Execute then
    begin
      if SaveDialog1.FileName='' then
        begin
          ShowMessage('Nombre de archivo en blanco. No se guardo nada.');
          Exit;
        end
      else
      begin
        ArchNuevo.SaveToFile(SaveDialog1.FileName);
      end;
    end;
end;

procedure TForm1.BGuardarLogClick(Sender: TObject);
var
  sd:TSaveDialog;
begin
  sd:=TSaveDialog.Create(nil);
  if sd.Execute then
    begin
      if sd.FileName='' then
        begin
          ShowMessage('Nombre de archivo en blanco. No se guardo nada.');
          FreeAndNil(sd);
          Exit;
        end
      else
      begin
        mmChequeando.Lines.SaveToFile(sd.FileName);
      end;
    end;
  FreeAndNil(sd);
end;

function TForm1.ChequearEnlace(const enlace: String; var sc: Integer): Boolean;
var
  http:TFPHTTPClient;
  httpstate:integer;
begin
  ECProgressBar1.Position:=0;
  lWeb.Caption:=enlace;
  mmChequeando.Lines.Add('-------------------------------------------------------------------------------');
  try
    http:=tfphttpclient.Create(nil);
    http.AllowRedirect:=True;
    http.OnRedirect:=@ShowReridect;
    http.OnDataReceived:=@DoOnDataReceived;
    if ECSlider1.Position>0 then http.IOTimeout:=trunc(ECSlider1.Position)*1000;
    mmChequeando.Lines.Add('Conectando a '+enlace);
    try
      http.Get(enlace);
      httpstate:=http.ResponseStatusCode;
      if ((httpstate>100) and (httpstate<400)) then result:=true else result:=false;
    except
      on E: Exception do
      begin
        mmChequeando.Lines.Add('Exception: '+E.Message);
        httpstate:=http.ResponseStatusCode;
        result:=false;
      end;
    end;
  finally
    mmChequeando.Lines.Add('Status: '+IntToStr(httpstate));
    Application.ProcessMessages;
    http.Free;
  end;
  sc:=httpstate;
  ECProgressBar1.Position:=ECProgressBar1.Max;
  Application.ProcessMessages;
end;

procedure TForm1.DesHabilitoBotones;
begin
  BChequearURLs.Enabled:=False;
  BVerResultados.Enabled:=False;
  BBuscarDuplicados.Enabled:=False;
  BGuardar.Enabled:=False;
  BGuardarComo.Enabled:=False;
  mnGuardar.Enabled:=False;
  mnGuardarComo.Enabled:=False;
  BGuardarLog.Enabled:=False;
end;

procedure TForm1.mnCreditosClick(Sender: TObject);
var
  sl:TStringList;
begin
  sl:=TStringList.Create;
  sl.add('IDE Lazarus: https://www.lazarus-ide.org/');
  sl.Add('Compilador Free Pascal: https://www.freepascal.org/');
  sl.Add('GNU: https://www.gnu.org/');
  sl.Add('Linux Mint: https://linuxmint.com/');
  sl.Add('Eye-Candy Controls: https://wiki.freepascal.org/Eye-Candy_Controls');
  sl.Add('Íconos: https://icons8.com/');
  sl.Add('FireFox: https://www.mozilla.org/es-AR/firefox/new/');
  sl.Add('DuckDuckGo: https://duckduckgo.com/');
  sl.Add('');
  VerMemo(sl);
  sl.Free;
end;

procedure TForm1.mnAcercaDeClick(Sender: TObject);
var
  sl:TStringList;
begin
  sl:=TStringList.Create;
  sl.add('Autor: Gastón Adrián Nieto');
  sl.Add('Correo electrónico: gaston@gansistemas.com');
  sl.Add(' ');
  sl.Add('Licencia: Copyleft. Este programa es de libre distribución.');
  sl.Add('Funciona sobre sistemas operativos GNU/Linux x86 x64 bits.');
  sl.Add('Garantía: ninguna.');
  sl.Add('Para marcadores de FireFox exportados en HTML.');
  sl.Add('');
  sl.Add('');
  VerMemo(sl);
  sl.Free;
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  FreeAndNil(ArchOrig);
  FreeAndNil(ArchTemp);
  FreeAndNil(ArchNuevo);
  CloseAction:=caFree;
end;

end.

