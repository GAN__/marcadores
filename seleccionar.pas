unit seleccionar;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Grids, globales, LCLIntf, dateutils,
  ExtCtrls, Buttons, StdCtrls;

type

  { TFSeleccionar }

  TFSeleccionar = class(TForm)
    BBorrar: TBitBtn;
    BCerrar: TBitBtn;
    BBuscarDuplicados: TBitBtn;
    BErrores: TBitBtn;
    BActualizar: TBitBtn;
    BVerPagina: TBitBtn;
    cbTodo: TCheckBox;
    lChequeo: TLabel;
    Memo1: TMemo;
    Panel1: TPanel;
    SGrid: TStringGrid;
    procedure BActualizarClick(Sender: TObject);
    procedure BBorrarClick(Sender: TObject);
    procedure BBuscarDuplicadosClick(Sender: TObject);
    procedure BCerrarClick(Sender: TObject);
    procedure BErroresClick(Sender: TObject);
    procedure BVerPaginaClick(Sender: TObject);
    procedure cbTodoChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure SGridDblClick(Sender: TObject);
    procedure SGridPrepareCanvas(sender: TObject; aCol, aRow: Integer; aState: TGridDrawState);
  private

  public
    function CargarGrid:Boolean;
  end;

var
  FSeleccionar: TFSeleccionar;

implementation

{$R *.lfm}

{ TFSeleccionar }

procedure TFSeleccionar.FormCreate(Sender: TObject);
begin
  lChequeo.Visible:=Not(Chequeo);
  SGrid.AllowOutboundEvents:=False;
  if not CargarGrid then exit;
  if not(Chequeo) then BErrores.Enabled:=False;
  if QueBuscar=Duplicados then BErrores.Enabled:=False;
  Memo1.Lines.Add('Total de enlaces: '+IntToStr(SGrid.RowCount-1));
end;

procedure TFSeleccionar.SGridDblClick(Sender: TObject);
begin
  if SGrid.Col<>2 then Exit;
  OpenURL(SGrid.Cells[2,SGrid.Row]);
end;

procedure TFSeleccionar.BVerPaginaClick(Sender: TObject);
begin
  if SGrid.RowCount<1 then Exit;
  OpenURL(SGrid.Cells[2,SGrid.Row]);
end;

procedure TFSeleccionar.cbTodoChange(Sender: TObject);
var
  i:Integer=0;
  s:String;
begin
  if cbTodo.Checked then s:='1' else s:='0';
  for i:=1 to SGrid.RowCount-1 do SGrid.Cells[0,i]:=s;
  SGrid.Repaint;
end;

procedure TFSeleccionar.BBorrarClick(Sender: TObject);
var
  i, borrados:Integer;
begin
  borrados:=0;
  for i:=SGrid.RowCount-1 downto 1 do
    if SGrid.Cells[0,i]='1' then
      begin
        aReg[(StrToInt(SGrid.Cells[5,i]))].Eliminar:=True;
        SGrid.DeleteRow(i);
        Inc(borrados);
      end;
  if borrados>0 then SeBorroAlgo:=True;
  Memo1.Lines.Add('Borrados: '+borrados.ToString);
end;

procedure TFSeleccionar.BActualizarClick(Sender: TObject);
var
  i:Integer;
begin
  SGrid.RowCount:=1;
  QueBuscar:=Todos;
  SGrid.Columns[3].Visible:=True;
  SGrid.Columns[4].Visible:=True;
  if not CargarGrid then exit;
  for i:=1 to SGrid.RowCount-1 do SGrid.Cells[0,i]:='0';
  SGrid.Repaint;
  Memo1.Lines.Add('Total de enlaces: '+IntToStr(SGrid.RowCount-1));
end;

procedure TFSeleccionar.BBuscarDuplicadosClick(Sender: TObject);
var
  i,j,cantfilas,cantduplicados:Integer;
  t:TMedirTiempo;
  fs:TFormatSettings;
begin
  fs:=DefaultFormatSettings;
  fs.LongTimeFormat:='hh:nn:ss.zzz';
  t:=TMedirTiempo.Create(nil);
  t.Iniciar;
  cantduplicados:=0;
  SGrid.RowCount:=1;
  QueBuscar:=Duplicados;
  if not CargarGrid then exit;
  SGrid.SortColRow(True,2);
  cantfilas:=SGrid.RowCount-1;
  for i:=1 to cantfilas-1 do
    begin
    for j:=i+1 to cantfilas do
      begin
      if SGrid.Cells[2,i]=SGrid.Cells[2,j] then
        begin
          SGrid.Cells[3,i]:='-1';
          SGrid.Cells[3,j]:='-2';
          aReg[StrToInt(SGrid.Cells[1,j])].borrar:=True;
          SGrid.Cells[0,j]:='1';
          Inc(cantduplicados);
        end
      else break;
      end;
    end;
  for i:=cantfilas downto 1 do
    if StrToInt(SGrid.Cells[3,i])>=0 then SGrid.DeleteRow(i);
  SGrid.Columns[3].Visible:=False;
  SGrid.Columns[4].Visible:=False;
  t.Finalizar;
  Memo1.Lines.Add('Duplicados: '+IntToStr(cantduplicados));
  Memo1.Lines.Add('Tiempo transcurrido: '+TimeToStr(t.Transcurrido,fs));
end;

procedure TFSeleccionar.BErroresClick(Sender: TObject);
begin
  SGrid.Columns[3].Visible:=True;
  SGrid.Columns[4].Visible:=True;
  QueBuscar:=Errores;
  SGrid.RowCount:=1;
  if not CargarGrid then exit;
  SGrid.SortColRow(True,3);
  Memo1.Lines.Add('Errores: '+IntToStr(SGrid.RowCount-1));
end;

procedure TFSeleccionar.SGridPrepareCanvas(sender: TObject; aCol, aRow: Integer; aState: TGridDrawState);
var
  scode, sredcode:Word;
  aColor:TColor;
begin
  if aRow<1 then Exit;
  if gdSelected in aState then
    begin
      aColor:=RGBToColor(9, 186, 9);
      SGrid.Canvas.Brush.Color:=RGBToColor(72, 72, 72);
      SGrid.Canvas.Font.Color:=aColor;
      SGrid.Canvas.Font.Style:=[fsBold];
      Exit;
    end;
  if QueBuscar=Duplicados then
  begin
    if SGrid.Cells[0,aRow]='1' then
    begin
      aColor:=RGBToColor(241, 83, 21);;
      SGrid.Canvas.Brush.Color:=aColor;
      SGrid.Canvas.Font.Color:=clWhite;
    end
    else
    begin
      aColor:=clDefault;
      SGrid.Canvas.Brush.Color:=aColor;
      SGrid.Canvas.Font.Color:=clBlack;
    end;
    Exit;
  end;
  if (QueBuscar=Todos) or (QueBuscar=Errores) then
  begin
    scode:=aReg[StrToInt(SGrid.Cells[1,aRow])].statuscode;
    case scode of
        0..99    : aColor:=RGBToColor(238, 231, 37);
        100..399 : aColor:=RGBToColor(54, 161, 49);
        400..499 : aColor:=RGBToColor(240, 186, 108);
        500..999 : aColor:=RGBToColor(252, 85, 68);
    else
      aColor:=clWhite;
    end;
    sredcode:=aReg[StrToInt(SGrid.Cells[1,aRow])].Redirect;
    if sredcode>=300 then aColor:=RGBToColor(119, 226, 35);
    SGrid.Canvas.Brush.Color:=aColor;
    SGrid.Canvas.Font.Color:=clBlack;
  end;
end;

function TFSeleccionar.CargarGrid: Boolean;
var
  i,f:Integer;
begin
  f:=0;
  for i:=Low(aReg) to High(aReg) do
  begin
    case QueBuscar of
      Todos : begin
                if ((aReg[i].chequear) and (not(aReg[i].Eliminar))) then
                 begin
                   Inc(f);
                   SGrid.InsertRowWithValues(f,['','','','']);
                   if aReg[i].borrar then SGrid.Cells[0,f]:='1' else SGrid.Cells[0,f]:='0';
                   SGrid.Cells[1,f]:=IntToStr(aReg[i].indice);
                   SGrid.Cells[2,f]:=aReg[i].URL;
                   SGrid.Cells[3,f]:=IntToStr(aReg[i].statuscode);
                   SGrid.Cells[4,f]:=IntToStr(aReg[i].Redirect);
                   SGrid.Cells[5,f]:=IntToStr(i);
                 end;
              end;
      Errores : begin
                  if ((aReg[i].chequear)  and (not(aReg[i].Eliminar)) and ((aReg[i].statuscode=0) or (aReg[i].statuscode>=400))) then
                  begin
                    Inc(f);
                    SGrid.InsertRowWithValues(f,['','','','']);
                    if aReg[i].borrar then SGrid.Cells[0,f]:='1' else SGrid.Cells[0,f]:='0';
                    SGrid.Cells[1,f]:=IntToStr(aReg[i].indice);
                    SGrid.Cells[2,f]:=aReg[i].URL;
                    SGrid.Cells[3,f]:=IntToStr(aReg[i].statuscode);
                    SGrid.Cells[4,f]:=IntToStr(aReg[i].Redirect);
                    SGrid.Cells[5,f]:=IntToStr(i);
                  end;
                end;
      Duplicados : begin
                     if ((aReg[i].chequear) and (not(aReg[i].Eliminar))) then
                       begin
                         Inc(f);
                         SGrid.InsertRowWithValues(f,['','','','']);
                         SGrid.Cells[0,f]:='0';
                         SGrid.Cells[1,f]:=IntToStr(aReg[i].indice);
                         SGrid.Cells[2,f]:=aReg[i].URL;
                         SGrid.Cells[3,f]:=IntToStr(aReg[i].statuscode);
                         SGrid.Cells[4,f]:=IntToStr(aReg[i].Redirect);
                         SGrid.Cells[5,f]:=IntToStr(i);
                       end;
                     end;
    end;
  end;
  Result:=SGrid.RowCount>0;
end;

procedure TFSeleccionar.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CloseAction:=caFree;
end;

procedure TFSeleccionar.BCerrarClick(Sender: TObject);
begin
  Close;
end;

end.


